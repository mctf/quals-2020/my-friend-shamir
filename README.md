# My Friend Shamir

## Описание
Просто забери секрет  
nc shamir.mctf.online 4444

К таску приложен файл с исходным кодом:
```
#!/usr/bin/ruby

require 'digest'
require 'consistent_hash'

def bin_to_hex(s)
    s.each_byte.map { |b| b.to_s(16) }.join
end

flag = ENV['FLAG']
p = 2**127 - 1
m = bin_to_hex(flag).hex

raise "Incorrect flag" unless m < p

puts("Enter your name:")

name = gets.strip
name_hash = Digest::SHA2.hexdigest name
rand = Random.new(name_hash.consistent_hash)

puts("Hey, #{name}! I have a secret that I need to store safely.")
sleep(1)
puts("But no one is 100% trustworthy")
sleep(1)
puts("That's why I decided to split the secret into two parts.")
sleep(1)
puts("I will give one part to you and the other to my friend Shamir.")
sleep(1)
puts("Here are my calculations:")
sleep(1)

# k = 2
a = rand(2..(p - 1))

x = []
k = []

puts("p = #{p}")
sleep(1)

x[0] = rand.rand(600000)
k[0] = (a*x[0] + m) % p
puts("x0 = #{x[0]}, k0 = #{k[0]}")
sleep(1)

x[1] = rand.rand(600000)
k[1] = (a*x[1] + m) % p

puts("And part for x1 = #{x[1]} I will give to Shamir.")
sleep(1)
puts("You surely won't be able to know his part.")
sleep(1)
puts("Thanks for your help! Come back again.")
```

## Принцип работы
Таск основан на [схеме разделения секрета Шамира](https://ru.wikipedia.org/wiki/%D0%A1%D1%85%D0%B5%D0%BC%D0%B0_%D1%80%D0%B0%D0%B7%D0%B4%D0%B5%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F_%D1%81%D0%B5%D0%BA%D1%80%D0%B5%D1%82%D0%B0_%D0%A8%D0%B0%D0%BC%D0%B8%D1%80%D0%B0) 
с параметрами k = 2, n = 2. Эта схема позволяет безопасно разделять секретную информацию m на n частей с гарантией 
восстановления секрета с использованием любых k частей. Для этого строится многочлен k - 1 степени F(x)
(в нашем случае многочлен 1 степени - прямая), и на ней случайным образом выбираются n точек k_i = F(x_i).
После этого пары (x_i, k_i) выдаются хранителям секрета.

## Уязвимость
В описанном выше алгоритме есть загвоздка: ни один из x_i не может равняться нулю. Иначе одна из частей получится 
x = 0, k = (0 + m) % p = m (гарантируется, что m < p). Из кода приложения видно, что x_0 выбирается с использованием 
ГПСЧ ruby, который инициализируется `sha256(name.strip).consistent_hash`.

Из свойств ГПСЧ можно узнать, что при его инициализации одним и тем же значением он всегда будет выдавать одну и ту же последовательность чисел. 
При этом пространство чисел, которое выдает рандом для x_0 достаточно маленькое, и его можно забрутить локально.
Осталось только поставить локально гем [consistent_hash](https://github.com/ucarion/consistent_hash) и подобрать имя, при 
котором рандом в нужный момент будет выдавать 0.

```
#!/usr/bin/ruby

require 'digest'
require 'consistent_hash'

for i in 0..10000000
    val = Digest::SHA2.hexdigest i.to_s
    rand = Random.new(val.consistent_hash)
    x = rand.rand(600000)
    if x == 0 then
        puts("#{i} #{val} #{val.consistent_hash}")
        break
    end
end
```

Пример такого имени: 112575. Отправим его на сервер и получим десятичное представление hex символов флага.
```
❯ nc shamir.mctf.online 4444
Enter your name:
112575
Hey, 112575! I have a secret that I need to store safely.
But no one is 100% trustworthy
That's why I decided to split the secret into two parts.
I will give one part to you and the other to my friend Shamir.
Here are my calculations:
p = 170141183460469231731687303715884105727
x0 = 0, k0 = 33854099411643937439882506877
And part for x1 = 427674 I will give to Shamir.
You surely won't be able to know his part.
Thanks for your help! Come back again.
```

Переведем его в символьную форму:
```
flag = 33854099411643937439882506877
puts([flag.to_s(16)].pack('H*'))

# mctf{sh4m1r}
```